// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "EDNAPush",
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "PushServerAPI",
            targets: ["PushServerAPI"]),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .binaryTarget(
            name: "PushServerAPI",
            url: "https://maven-pub.edna.ru/repository/maven-public/com/mfms/ios/push/2.6.0/edna-push-full-2.6.0.zip",
            checksum: "96320c5af4705d829d345b92ed2412188d27f07f29e5d53137a04ce3c01b9067"
        ),
    ]
)
